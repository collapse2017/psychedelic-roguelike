#include "Tile.h"

Tile::Tile()
{
    sym = '#';
}
  
void Tile::setSym(char newSym)
{
    sym = newSym;
}

char Tile::getSym()
{
    return sym;
}