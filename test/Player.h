#ifndef PLAYER_H
#define PLAYER_H

// testing purposes
#include <iostream>
using namespace std;

#include "Character.h"



class Player: public Character {

public:
  Player(int x, int y) 
  {
	  setX(x);
	  setY(y);
  }
  
  void attack();
  void move();

  /*void attachMap(Map &m) {
	  this->curMap = m;
  }*/
};

#endif