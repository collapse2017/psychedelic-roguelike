#include <ncurses.h>
#include "Tile.h"

Tile::Tile()
{
  sym = '#';
}

char Tile::getSym()
{
  return sym;
}

void Tile::setSym(char newSym)
{
  sym = newSym;
}
