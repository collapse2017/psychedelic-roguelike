#ifndef PLAYER_H
#define PLAYER_H


#include "Character.h"



class Player: public Character {

public:
  Player(int x, int y, Map* map)
  {
    setX(x);
    setY(y);
    setMap(map);

    Tile t;
    t.setSym('@');
    setTile(t);
  }
  
  void attack();
  void move();

};

#endif
