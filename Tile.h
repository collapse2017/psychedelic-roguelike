#ifndef TILE_H
#define TILE_H

class Tile
{
 private:
  char sym;

 public:
  Tile();
  char getSym();
  void setSym(char newSym);
};


#endif
