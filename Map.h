#ifndef MAP_H
#define MAP_H
#include "Tile.h"

class Map
{
 private:
  Tile map[50000];
  char cmap[50000];
  
  void mapGen();
  void drawRoom(int length, int width, int curX, int curY);
  bool checkRoom(int curX, int curY, int hallL, int hallW, int dir, int eHallX, int eHallY, int roomW, int roomL);

 public:

  Map();
  void drawMap(int x, int y);
  Tile * getMap();
  void setTile(int place, Tile newTile);
  
};

#endif
