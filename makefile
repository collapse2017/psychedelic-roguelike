all: game.exe

game.exe : Map.o Game.o Tile.o Player.o
	g++ -std=c++11 Map.o Game.o Tile.o Player.o -lncurses -o game.exe

Map.o : Map.cpp
	g++ -std=c++11 -c Map.cpp

Tile.o : Tile.cpp
	g++ -c Tile.cpp
        
Player.o : Player.cpp
	g++ -c Player.cpp

clean :
	rm *o game.exe
