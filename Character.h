#include <ncurses.h>

#include "Tile.h"
#include "Map.h"

class Character
{
protected:
  int x, y;
  Map* curMap;
  Tile curTile;
  
public:

  virtual void attack() = 0;
  virtual void move() = 0;

  void setMap(Map *m) {this->curMap = m;}  
  void setTile(Tile t) {this->curTile = t;}
  void setX(int x) {this->x = x;}
  void setY(int y) {this->y = y;}
  
  Tile getTile() {return curTile;}
  int getX() {return x;}
  int getY() {return y;} 
  
};
