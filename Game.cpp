#include <ncurses.h>
#include "Player.h"
#include "Map.h"



int main()
{
	initscr();
	noecho();

        Map tmap;
	
        Player p(9,9,&tmap);
	tmap.setTile(p.getX()+(400*p.getY()), p.getTile());
	tmap.drawMap(5,5);
	refresh();
	getch();
	endwin();

	return 0;
}
