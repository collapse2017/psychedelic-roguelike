#include <ncurses.h>
#include <stdlib.h>
#include <time.h>
#include <random>
#include <functional>

#include "Map.h"
  
void Map::mapGen()
{
  bool done, ok;
  int len, wi, x, y, d, nx, ny, ldir, ndir, llen, lwi, oldx, oldy;
  int hW, hL, nRX, nRY;
  std::mt19937::result_type seed = time(0);
  auto rmrand = std::bind(std::uniform_int_distribution<int>(10,20), std::mt19937(seed));
  auto hallrand = std::bind(std::uniform_int_distribution<int>(8,15), std::mt19937(seed));
  auto hallwrand = std::bind(std::uniform_int_distribution<int>(3,5), std::mt19937(seed));
  auto nhallrand = std::bind(std::uniform_int_distribution<int>(0,2), std::mt19937(seed));
  auto drand = std::bind(std::uniform_int_distribution<int>(0,3), std::mt19937(seed));
  
  x = 7;
  y = 7;
  int counter = 0;
  done = false;
  ok = true;
  len = rmrand();
  wi = rmrand();
  
  drawRoom(len, wi, x, y);
  
  x += len;
  y += wi;
  llen = len;
  lwi = wi;
  
  while (counter < 25000)
    {
      counter++;
      oldx = x;
      oldy = y;
      
      len = rmrand();
      wi = rmrand();
      hL = hallrand();
      hW = hallwrand();
      
      ndir = drand();
      
      auto llenrand = std::bind(std::uniform_int_distribution<int>(5,llen), std::mt19937(seed));
      auto lwirand = std::bind(std::uniform_int_distribution<int>(5,lwi), std::mt19937(seed));
      auto lenrand = std::bind(std::uniform_int_distribution<int>(0,(len-6)), std::mt19937(seed));
      auto wirand = std::bind(std::uniform_int_distribution<int>(0,(wi-6)), std::mt19937(seed));
      
      switch(ndir)
	{
	case 0:
	  {
	    x = x - llenrand();
	    y = y - lwi + 1;
	    nx = lenrand();
	    ny = y - hL;
	    
	    if ((y - hL - wi) < 0)
	      break;
	    
	    if ((x + (len-nx)) > 400)
	      break;
	    
	    if ((x - nx) < 0)
	      break;
	    
	    if (checkRoom(x, y, hL, hW, ndir, nx, ny, wi, len))
	      {
		drawRoom(hW, hL, x, (y-hL));
		x = x - nx;
		y = y - wi - hL;
		drawRoom(len, wi, x, y);
		
		x += len;
		y += wi;
		llen = len;
		lwi = wi;
		
		counter = 0;
		break;
	      }
	    
	    else
	      {
		x = oldx;
		y = oldy;
		break;
	      }
	  }
	  
	case 1:
	  {
	    x = x - llen+1;
	    y = y - lwirand();
	    ny = wirand();
	    nx = x - (hL+1);
	    
	    if ((x - hL - len) < 0)
	      break;
	    
	    if ((y - ny) < 0)
	      break;
	    
	    if ((y + (wi - ny)) > 125)
	      break;
	    
	    if (checkRoom(x, y, hL, hW, ndir, nx, ny, wi, len))
	      {
		drawRoom(hL, hW, (x - hL), y);
		x = x - hL - len - 1;
		y = y - ny;
		drawRoom(len, wi, x, y);
		
		x += len;
		y += wi;
		llen = len;
		lwi = wi;
		
		counter = 0;
		break;
	      }
	    
	    else
	      {
		x = oldx;
		y = oldy;
		break;
	      }
	  }
	  
	case 2:
	  {
	    x = x - llenrand();
	    ny = y + hL + 1;
	    nx = lenrand();
	    
	    if ((y + hL + wi) > 125)
	      break;
	    
	    if ((x - nx) < 0)
	      break;
	    
	    if ((x + (len-nx)) > 400)
	      break;
	    
	    if (checkRoom(x, y, hL, hW, ndir, nx, ny, wi, len))
	      {
		drawRoom(hW, hL, x, y);
		x = x - nx;
		y = y + (hL+1);
		drawRoom(len, wi, x, y);
		
		x += len;
		y += wi;
		llen = len;
		lwi = wi;
		
		counter = 0;
		break;
	      }
	    
	    else
	      {
		x = oldx;
		y = oldy;
		break;
	      }
	  }
	  
	case 3:
	  {
	    y = y - lwirand();
	    ny = wirand();
	    nx = x + hL + 1;
	    
	    if ((x + hL + len) > 400)
	      break;
	    
	    if ((y - ny) < 0)
	      break;
	    
	    if ((y + (wi-ny)) > 125)
	      break;
	    
	    if (checkRoom(x, y, hL, hW, ndir, nx, ny, wi, len))
	      {
		drawRoom(hL, hW, x, y);
		x = x + hL + 1;
		y = y - ny;
		drawRoom(len, wi, x, y);
		
		x += len;
		y += wi;
		llen = len;
		lwi = wi;
		
		counter = 0;
		break;
	      }
	    
	    else
	      {
		x = oldx;
		y = oldy;
		break;
	      }
	  }
	}
    }
}

void Map::drawRoom(int length, int width, int curX, int curY)
{
  int cpos = (curY * 400 + curX);
  for (int i = 0; i < length; i++)
    {
      map[cpos + i].setSym('#');
      cmap[cpos + i] = '#';
    }
  cpos+=400;
  
  for (int i = 0; i < width-1; i++)
    {
      map[cpos].setSym('#');
      cmap[cpos] = '#';
      for (int j = 1; j < length-1; j++)
	{
	  map[cpos + j].setSym('.');
	  cmap[cpos + j] = '.';
	}
      map[cpos + length - 1].setSym('#');
      cmap[cpos + length - 1] = '#';
      cpos += 400;
    }
  
  for (int i = 0; i < length; i++)
    {
      map[cpos + i].setSym('#');
      cmap[cpos + i] = '#';
    }
  cpos += (400 + length);
}


bool Map::checkRoom(int curX, int curY, int hallL, int hallW, int dir, int eHallX, int eHallY, int roomW, int roomL)
{
  bool result = true;
  int curPos = (400*curY + curX);
  switch(dir)
    {
    case 0:
      {
	for (int i = 1; i < hallL; i++)
	  {
	    if (cmap[(curPos - 400*i)] != 'x')
	      result = false;
	    
	    if (cmap[(curPos - 400*i + hallW)] != 'x')
	      result = false;
	  }
	
	if (result == true)
	  {
	    curPos = curPos - ((hallL+1)*400);
	    if (cmap[(curPos - eHallX)] != 'x')
	      result = false;
	    
	    if (cmap[((curPos - eHallX) - 400*roomW)] != 'x')
	      result = false;
	    
	    if (cmap[(curPos + (roomL - eHallX))] != 'x')
	      result = false;
	    
	    if (cmap[(curPos + (roomL - eHallX) - (400 * roomW))] != 'x')
	      result = false;
	  }
	break;
      }
      
    case 1:
      {
	for (int i = 1; i < hallL; i++)
	  {
	    if (cmap[(curPos - i)] != 'x')
	      result = false;
	    
	    if (cmap[(curPos - i + 400*hallW)] != 'x')
	      result = false;
	  }
	
	if (result == true)
	  {
	    curPos = curPos - (hallL+1);
	    if (cmap[(curPos - eHallY*400)] != 'x')
	      result = false;
	    
	    if (cmap[((curPos - 400*eHallY) - roomL)] != 'x')
	      result = false;
	    
	    if (cmap[(curPos + (roomW - eHallY)*400)] != 'x')
	      result = false;
	    
	    if (cmap[(curPos + (roomW - eHallY)*400 - roomL)] != 'x')
	      result = false;
	  }
	break;
      }
      
    case 2:
      {
	for (int i = 1; i < hallL; i++)
	  {
	    if (cmap[(curPos + 400*i)] != 'x')
	      result = false;
	    
	    if (cmap[(curPos + 400*i + hallW)] != 'x')
	      result = false;
	  }
	
	if (result == true)
	  {
	    curPos = curPos + ((hallL+1)*400);
	    if (cmap[(curPos - eHallX)] != 'x')
	      result = false;
	    
	    if (cmap[((curPos - eHallX) + 400*roomW)] != 'x')
	      result = false;
	    
	    if (cmap[(curPos + (roomL - eHallX))] != 'x')
	      result = false;
	    
	    if (cmap[(curPos + (roomL - eHallX) + (400 * roomW))] != 'x')
	      result = false;
	  }
	break;
      }
      
    case 3:
      {
	for (int i = 1; i < hallL; i++)
	  {
	    if (cmap[curPos + i] != 'x')
	      result = false;
	    if (cmap[(curPos + i + 400*hallW)] != 'x')
	      result = false;
	  }
	if (result == true)
	  {
	    curPos += hallL+1;
	    if (cmap[(curPos - 400 * eHallY)] != 'x')
	      result = false;
	    
	    if (cmap[(curPos - 400 * eHallY + roomL)] != 'x')
	      result = false;
	    
	    if (cmap[(curPos + 400 * (roomW - eHallY))] != 'x')
	      result = false;
	    
	    if (cmap[(curPos + 400 * (roomW - eHallY) + roomL)] != 'x')
	      result = false;
	  }
	break;
      }
    }
  
  return result;
}

Map::Map()
{
  for (int i = 0; i < 50000; i++)
    {
      map[i].setSym('x');
      cmap[i] = 'x';
    }
  mapGen();
}

void Map::drawMap(int x, int y)
{
  for (int i = 0; i < 25; i++)
    {
      for (int j = 0; j < 80; j++)
	{
	  addch(cmap[((y+i)*400 + (x + j))]);
	}
    }
}


Tile* Map::getMap()
{
  return map;
}

void Map::setTile(int place, Tile newTile)
{
  printf("newtile");
  map[place] = newTile;
  cmap[place] = newTile.getSym();
}
